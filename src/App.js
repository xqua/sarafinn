import React, {useEffect, useState} from 'react';
import './App.css';
import 'semantic-ui-css/semantic.min.css'
import { Progress, Container, Header, Statistic } from 'semantic-ui-react'

let conceptionDate = new Date('June 1, 2020 22:15:30');

// weight in gram
// height in cm
let statsTable = {
  0:  {weight: 0, vegetable: "None", height: 0},
  1:  {weight: 1, vegetable: "None", height: 1},
  10: {weight: 35, vegetable: "Kumquat", height: 3.2},
  11: {weight: 45, vegetable: "Lime", height: 4.2},
  12: {weight: 58, vegetable: "Plum", height: 5.3},
  13: {weight: 73, vegetable: "Lemon", height: 6.5},
  14: {weight: 93, vegetable: "Nectarine", height: 7.9},
  15: {weight: 117, vegetable: "Apple", height: 16.4},
  16: {weight: 146, vegetable: "Avocado", height: 18.3},
  17: {weight: 181, vegetable: "Pear", height: 20.1},
  18: {weight: 223, vegetable: "Bell Pepper", height: 22},
  19: {weight: 273, vegetable: "Heirloom tomato", height: 23.7},
  20: {weight: 331, vegetable: "Artichoke", height: 25.5},
  21: {weight: 399, vegetable: "Carrots", height: 27.2},
  22: {weight: 478, vegetable: "Papaya", height: 28.8},
  23: {weight: 568, vegetable: "Grapefruit", height: 30.4},
  24: {weight: 670, vegetable: "Ear of corn", height: 32},
  25: {weight: 785, vegetable: "Rutabaga", height: 33.6},
  26: {weight: 913, vegetable: "Lettuce", height: 35.1},
  27: {weight: 1055, vegetable: "Cauliflower", height: 36.5},
  28: {weight: 1210, vegetable: "Eggplant", height: 37.9},
  29: {weight: 1379, vegetable: "Acorn Squash", height: 39.3},
  30: {weight: 1559, vegetable: "Cabbage", height: 40.6},
  31: {weight: 1751, vegetable: "Coconut", height: 41.9},
  32: {weight: 1953, vegetable: "Jicama", height: 43.2},
  33: {weight: 2162, vegetable: "Pineapple", height: 44.4},
  34: {weight: 2377, vegetable: "Butternut Squash", height: 45.6},
  35: {weight: 2595, vegetable: "Honeydew", height: 46.7},
  36: {weight: 2813, vegetable: "Swiss Chard", height: 47.8},
  37: {weight: 3028, vegetable: "Winter Melon", height: 48.9},
  38: {weight: 3236, vegetable: "Rhubarb", height: 49.9},
  39: {weight: 3435, vegetable: "Yardlong beans", height: 50.9},
  40: {weight: 3619, vegetable: "Pumpkin", height: 52},
  41: {weight: 3787, vegetable: "Watermelon", height: 52.7},
  42: {weight: 3787, vegetable: "Jackfruit", height: 52.7}
}

function interpolate(week, metric) {
  const value1 = statsTable[Math.round(week)][metric];
  const value2 = statsTable[Math.round(week) + 1][metric];
  const value = value1 + (week - Math.round(week)) * ((value2 - value1) / ((Math.round(week) + 1) - Math.round(week)))
  return value
}

function App() {

  const [daysElapsed, setDaysElapsed] = useState(0);
  const [weeksElasped, setWeeksElapsed] = useState(0);

  useEffect(() => {
  // Update the document title using the browser API
    const interval = setInterval(() => {
      const now = Date.now();
      const timeElapsed = now - conceptionDate;
      const daysElapsed = timeElapsed / (1000 * 3600 * 24);
      setDaysElapsed(daysElapsed);
      const weeksElasped = daysElapsed / 7
      setWeeksElapsed(weeksElasped);
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  return (
    <div className="root">
    <Container textAlign="center" className="App-header">
      <Header
        as='h1'
        inverted
        style={{
          fontSize: '2em',
          fontWeight: 'normal',
          marginBottom: 0,
        }}
      >SaraFinn's Baby progress bar</Header>
      {
        Math.random() > 0.5 ?
        <Progress percent={((weeksElasped/40) * 100).toFixed(5)} color='violet' size='big' progress/>
        :
        <Progress percent={((weeksElasped/40) * 100).toFixed(5)} color='green' size='big' progress/>
      }
      <Header as="h2" style={{color: "white"}}>Baby is compiling... Please wait!</Header>
    </Container>
    <Container className="App-stats">
      <Statistic.Group>
      <Statistic>
        <Statistic.Value>{Math.round(daysElapsed)}</Statistic.Value>
        <Statistic.Label>Days since DNA mixing</Statistic.Label>
      </Statistic>
      <Statistic>
        <Statistic.Value>{Math.round(weeksElasped)}</Statistic.Value>
        <Statistic.Label>Current Week</Statistic.Label>
      </Statistic>
      <Statistic>
        <Statistic.Value>{statsTable[Math.round(weeksElasped)].vegetable}</Statistic.Value>
        <Statistic.Label>Equivalent vegetable</Statistic.Label>
      </Statistic>
      <Statistic>
        <Statistic.Value>{interpolate(weeksElasped, "weight").toFixed(2)}</Statistic.Value>
        <Statistic.Label>Estimated Weight (in g)</Statistic.Label>
      </Statistic>
      <Statistic>
        <Statistic.Value>{interpolate(weeksElasped, "height").toFixed(2)}</Statistic.Value>
        <Statistic.Label>Estimated Size (in cm)</Statistic.Label>
      </Statistic>
      <Statistic>
        <Statistic.Value>{Math.round(interpolate(weeksElasped, "weight")/0.00000001)}</Statistic.Value>
        <Statistic.Label>Estimated number of cells</Statistic.Label>
      </Statistic>
      </Statistic.Group>

      {
        (weeksElasped/3) * 100 > 100 ?
        <Progress percent={(weeksElasped/3) * 100} color='olive' label='Implantation Complete!'/>
        :
        <Progress percent={(weeksElasped/3) * 100} color='blue' label='Embryo looking for a nest ...'/>
      }

      {
        (weeksElasped/5) * 100 > 100 ?
        <Progress percent={(weeksElasped/5) * 100} color='olive' label='Heart Complete!'/>
        :
        <Progress percent={(weeksElasped/5) * 100} color='blue' label='Heart compiling ...'/>
      }

      {
        (weeksElasped/7) * 100 > 100 ?
        <Progress percent={(weeksElasped/7) * 100} color='olive' label='Movement acquired!'/>
        :
        <Progress percent={(weeksElasped/7) * 100} color='blue' label='Movement compiling ...'/>
      }

      {
        (weeksElasped/7) * 100 > 100 ?
        <Progress percent={(weeksElasped/7) * 100} color='olive' label='Fingerprints set!'/>
        :
        <Progress percent={(weeksElasped/7) * 100} color='blue' label='Fingerprints compiling ...'/>
      }

      {
        (weeksElasped/13) * 100 > 100 ?
        <Progress percent={(weeksElasped/13) * 100} color='olive' label='Organogenesis Complete!'/>
        :
        <Progress percent={(weeksElasped/13) * 100} color='blue' label='Organogenesis compiling ...'/>
      }

      {
        (weeksElasped/14) * 100 > 100 ?
        <Progress percent={(weeksElasped/14) * 100} color='olive' label='Toenails Complete!'/>
        :
        <Progress percent={(weeksElasped/14) * 100} color='blue' label='Toenails compiling ...'/>
      }

      {
        (weeksElasped/18) * 100 > 100 ?
        <Progress percent={(weeksElasped/18) * 100} color='olive' label='It can ear!'/>
        :
        <Progress percent={(weeksElasped/18) * 100} color='blue' label='Audition compiling ...'/>
      }

      {
        (weeksElasped/19) * 100 > 100 ?
        <Progress percent={(weeksElasped/19) * 100} color='olive' label='It has ears!'/>
        :
        <Progress percent={(weeksElasped/19) * 100} color='blue' label='Ears compiling ...'/>
      }

      {
        (weeksElasped/19) * 100 > 100 ?
        <Progress percent={(weeksElasped/19) * 100} color='olive' label='It has a nose!'/>
        :
        <Progress percent={(weeksElasped/19) * 100} color='blue' label='Nose compiling ...'/>
      }

      {
        (weeksElasped/19) * 100 > 100 ?
        <Progress percent={(weeksElasped/19) * 100} color='olive' label='It has lips!'/>
        :
        <Progress percent={(weeksElasped/19) * 100} color='blue' label='Lips compiling ...'/>
      }

      {
        (weeksElasped/20) * 100 > 100 ?
        <Progress percent={(weeksElasped/20) * 100} color='olive' label='It has hairs (Weird)!'/>
        :
        <Progress percent={(weeksElasped/20) * 100} color='blue' label='Hairs compiling ...'/>
      }

      {
        (weeksElasped/20) * 100 > 100 ?
        <Progress percent={(weeksElasped/20) * 100} color='olive' label='It makes antibodies!'/>
        :
        <Progress percent={(weeksElasped/20) * 100} color='blue' label='Immune system compiling ...'/>
      }

      {
        (weeksElasped/20) * 100 > 100 ?
        <Progress percent={(weeksElasped/20) * 100} color='olive' label='It dreams of electric sheeps (REM detectable)!'/>
        :
        <Progress percent={(weeksElasped/20) * 100} color='blue' label='Dream system compiling ...'/>
      }

      {
        (weeksElasped/25) * 100 > 100 ?
        <Progress percent={(weeksElasped/25) * 100} color='olive' label='Lungs Complete!'/>
        :
        <Progress percent={(weeksElasped/25) * 100} color='blue' label='Lungs compiling ...'/>
      }

      {
        (weeksElasped/28) * 100 > 100 ?
        <Progress percent={(weeksElasped/28) * 100} color='olive' label='It has eyelashes!'/>
        :
        <Progress percent={(weeksElasped/28) * 100} color='blue' label='Eyelashes compiling ...'/>
      }

      {
        (weeksElasped/33) * 100 > 100 ?
        <Progress percent={(weeksElasped/33) * 100} color='olive' label='It has big muscle!'/>
        :
        <Progress percent={(weeksElasped/33) * 100} color='blue' label='Muscles compiling ...'/>
      }

      {
        (weeksElasped/33) * 100 > 100 ?
        <Progress percent={(weeksElasped/33) * 100} color='olive' label='It has some fat!'/>
        :
        <Progress percent={(weeksElasped/33) * 100} color='blue' label='Bodyfat compiling ...'/>
      }

      {
        (weeksElasped/39) * 100 > 100 ?
        <Progress percent={(weeksElasped/39) * 100} color='olive' label='It has some shaky bones!'/>
        :
        <Progress percent={(weeksElasped/39) * 100} color='blue' label='Bones compiling ...'/>
      }
    </Container>
    </div>
  );
}

export default App;
